import { Module } from '@nestjs/common';
import { LivecoinService } from './livecoin.service';

@Module({
  providers: [LivecoinService],
})
export class LivecoinModule {}
