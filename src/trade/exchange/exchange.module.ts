import { Module } from '@nestjs/common';
import { LivecoinModule } from './livecoin/livecoin.module';

@Module({
  imports: [LivecoinModule],
})
export class ExchangeModule {}
