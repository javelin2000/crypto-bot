import { Test, TestingModule } from '@nestjs/testing';
import { LivecoinService } from './livecoin.service';

describe('LivecoinService', () => {
  let service: LivecoinService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LivecoinService],
    }).compile();

    service = module.get<LivecoinService>(LivecoinService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
