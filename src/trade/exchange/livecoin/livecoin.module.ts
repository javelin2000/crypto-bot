import { HttpModule, Module } from '@nestjs/common';
import { LivecoinService } from './livecoin.service';
import { LivecoinController } from './livecoin.controller';

@Module({
  imports: [HttpModule],
  providers: [LivecoinService],
  controllers: [LivecoinController],
})
export class LivecoinModule {}
