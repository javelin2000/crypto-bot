import { Dependencies, HttpService, Injectable } from '@nestjs/common';
import { url } from './config';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AxiosResponse } from 'axios';
import { IPairRate } from './interface';

@Injectable()
@Dependencies(HttpService)
export class LivecoinService {
  public constructor(private readonly httpService: HttpService) {}
  public getExchangeTicker(
    currencyFrom: string,
    curencyTo: string,
  ): Observable<IPairRate[]> {
    let requestParam!: { currencyPair: string };
    if (currencyFrom && curencyTo) {
      requestParam = {
        currencyPair: `${currencyFrom.toLocaleUpperCase()}/${curencyTo.toLocaleUpperCase()}`,
      };
    }
    const resp: Observable<IPairRate[]> = this.httpService
      .get(`${url}/exchange/ticker`, { params: requestParam })
      .pipe(
        map((response: AxiosResponse) => {
          const data: IPairRate[] = Array.isArray(response.data)
            ? response.data
            : [response.data];
          return data;
        }),
      );
    return resp;
  }
}
