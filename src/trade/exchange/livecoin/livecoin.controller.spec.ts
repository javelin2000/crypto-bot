import { Test, TestingModule } from '@nestjs/testing';
import { LivecoinController } from './livecoin.controller';

describe('Livecoin Controller', () => {
  let controller: LivecoinController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LivecoinController],
    }).compile();

    controller = module.get<LivecoinController>(LivecoinController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
