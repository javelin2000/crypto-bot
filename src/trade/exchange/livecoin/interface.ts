export interface IPairRate {
  symbol: string; // sample "BTC/USD"
  last: number; // sample 431.15098
  high: number; // sample  447
  low: number; // sample  420,
  volume: number; // sample  491.24533286,
  vwap: number; // sample  440.11749153,
  max_bid: number; // sample  447,
  min_ask: number; // sample  420,
  best_bid: number; // sample  429.26,
  best_ask: number; // sample  431.125
  rate?: number;
  cur?: string;
}
