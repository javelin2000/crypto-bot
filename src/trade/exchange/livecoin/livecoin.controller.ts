import { Controller, Get, Param, Query } from '@nestjs/common';
import { LivecoinService } from './livecoin.service';
import { ApiQuery } from '@nestjs/swagger';
import { Observable } from 'rxjs';
import { IPairRate } from './interface';

@Controller('livecoin')
export class LivecoinController {
  public constructor(private readonly livecoinService: LivecoinService) {}

  @ApiQuery({ name: 'from', required: false, type: String })
  @ApiQuery({ name: 'to', required: false, type: String })
  @Get('exchangeTicker')
  public getExchangeTicker(
    @Query('from') from?: string,
    @Query('to') to?: string,
  ): Observable<IPairRate[]> {
    return this.livecoinService.getExchangeTicker(from || '', to || '');
  }
}
